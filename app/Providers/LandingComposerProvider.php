<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LandingComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->composeLanding();
    }

    public function composeLanding(){
        view()->creator('*','App\Http\ViewComposers\LandingComposer');
    }
}
