<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::share('myname','Angad');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer(
            'layouts.admin',
            'App\Http\ViewComposers\AdminMenuComposer'
        );
        view()->composer(
            'layouts.app',
            'App\Http\ViewComposers\AdvertisementComposer'
        );
    }
}
