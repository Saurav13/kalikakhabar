<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class News extends Model
{
    protected $table = 'news';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public function category(){
            return $this->belongsTo('App\Category');
        }

        use Sluggable;
        
            /**
             * Return the sluggable configuration array for this model.
             *
             * @return array
             */
            public function sluggable()
            {
                return [
                    'slug' => [
                        'source' => 'title'
                    ]
                ];
            }
}




class Post extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}