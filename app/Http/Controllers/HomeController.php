<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\News;
use Counter;
use App\Subscriber;
use App\ContactUsMessage;
use Validator;
use Illuminate\Support\Str;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // dd(date('Y-m-d'));
        $headlines=News::where('date','>=',date('Y-m-d'))->pluck('title')->implode(' | ');
        // dd($headlines);
        $id=Category::first()->id;
        $news=News::where('category_id',$id)->where('date','>=',date('Y-m-d'))->orderBy('date','desc')->limit(10)->get();
        Counter::count('home'); 
        return view('frontend.landing')->with('categories',Category::has('news')->get())->with('headlines',$headlines)->with('news',$news);
    }

    public function singlenews($slug)
    {
        $headlines=News::where('date','>=',date('Y-m-d'))->pluck('title')->implode(' | ');
        $news = News::where('slug','=',$slug)->first();
     
        
        return view('frontend.singlenews')->with('news', $news)->with('headlines',$headlines);
    }

    
    public function news($name)
    {
        $headlines=News::where('date','>=',date('Y-m-d'))->pluck('title')->implode(' | ');

        Counter::count($name);
        $cat_id = Category::where('name','=',$name)->first()->id;
        $news = News::where('category_id', '=', $cat_id)->orderby('date','desc')->paginate(3);
        $cat_name = Category::where('name','=',$name)->first()->name;
    
        return view('frontend.news')->with('news', $news)->with('cat_name',$cat_name)->with('headlines',$headlines);
    }

    public function allnews()
    {
        Counter::count('all');
        $news = News::join('categories','news.category_id','categories.id')->select('news.*','categories.name')->orderBy('id','desc')->get();

        return view('frontend.allnews')->with('news', $news);
    }

    public function test(){
        return view('test');
    }

    public function subscribe(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            Session::flash('error','Please provide a valid email and Try again.');
            return redirect()->back();
        }

        if (! Subscriber::where('email', '=', $request->email)->exists()) {

            $subscriber = new Subscriber();
            $subscriber->email = $request->email;
            $subscriber->token = Str::random(60);
            $subscriber->save();
            Session::flash('success','You have been successfully subscribed to us.');
        }
        else
            Session::flash('error','Looks like you are already in our subscription list.');
        return redirect()->back();
    }

    public function unsubscribe($token){
        $subscriber = Subscriber::where('token','=',$token)->first();
        
        if(!$subscriber) abort(404);
        return view('frontend.unsubscribe')->with('token',$token);
    }

    public function unsubscribed($token)
    {
        $subscriber = Subscriber::where('token','=',$token)->first();
        
        if(!$subscriber) abort(404);
        $subscriber->delete();
        Session::flash('success','You are no longer subscribed to us.');
        return redirect()->route('home');
    }

    public function contact_us(Request $request){
        return view('frontend.contact');
    }

    public function contactSend(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:191',
            'email'=> 'required|email',
            'message'=> 'required|max:191',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('error', 'Please Provide All Necessary Information.');
            return redirect()->route('contact');
        }
        $contact_message = new ContactUsMessage;
        $contact_message->name = $request->name;
        $contact_message->email = $request->email;
        $contact_message->message = $request->message;
        $contact_message->save();
        
        $request->session()->flash('success', "We'll respond to you soon.");
        return redirect()->route('contact');        
    }

}
