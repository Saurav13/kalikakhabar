<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class KhabarController extends Controller
{
    public function index(){
        $items = Category::all();
        return view('index')->with('items', $items);

    }

    public function store(Request $request)
    {
      
        //create post
        $item = new Category;
        $item->name = $request->input('name');
        $item->save();
        
        return redirect('admin');
    }

    public function destroy($id)
    {
        $item = Categories::find($id);
        $item->delete();
        return redirect('admin.dashboard');
    }
}
