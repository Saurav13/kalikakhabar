<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Gallery;
use Counter;

class GalleryController extends Controller
{
    public function index(){
        $album = Album::has('album_images')->get();
        Counter::count('gallery');
        // $news = News::join('categories','news.category_id','categories.id')
        // ->select('news.*','categories.name')->orderBy('id','desc')->get();
        return view('frontend.gallery')->with('album',$album);
    }
}
