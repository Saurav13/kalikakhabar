<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Category;
use App\News;
use Image;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $items= Category::all();
        // $news = DB::table('news')->paginate(2);
        $news= News::orderBy('id', 'DESC')->paginate(10);
        return view('admin/news')->with('items', $items)->with('news', $news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(array(
            'category_id' => 'required',
            'title' => 'required',
            'news_image' => 'image|required|max:1999',
            'body1' => 'required',
            'newsdate' => 'required'
        ),array(
            'category_id.required' => 'Category name is required',
            'title.required' => 'Title is required',
            'news_image.required' => 'News Image is required',
            'body1.required' => 'News First Paragraph is required',
            'newsdate.required' => 'News Date is required'
        ));

        

        //Handle file upload
        if($request->hasFile('news_image')){
            //Get filemame with the extension
            $filenameWithExt = $request->file('news_image')->getClientOriginalName();
            //Get hust filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just ext
            $extension = $request->file('news_image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('news_image')->storeAs('public/news_image', $fileNameToStore);
           
            $img = Image::make($request->file('news_image'))->resize(730, 490)->encode($extension);
            Storage::put('public/news_image_large'.'/'.$fileNameToStore,$img);
            
            $img = Image::make($request->file('news_image'))->resize(400, 270)->encode($extension);
            Storage::put('public/news_image_medium'.'/'.$fileNameToStore,$img);

            $img = Image::make($request->file('news_image'))->resize(70, 50)->encode($extension);
            Storage::put('public/news_image_small'.'/'.$fileNameToStore,$img);
           
        }

        //create news
        $news = new News;
        $news->date = $request->input('newsdate');
        $news->category_id = $request->input('category_id');
        $news->title = $request->input('title');
        $news->body1 = $request->input('body1');
        $news->body2 = $request->input('body2');
        $news->media_url = $request->input('media_url');
        if($request->hasFile('news_image'))
        {
            $news->news_image = $fileNameToStore;
        }
        $news->save();

        return redirect('/admin/news')->with('success', 'News Created');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        $items = Category::all();
        return view('admin/newsedit')->with('news', $news)->with('items', $items);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate(array(
            'category_id' => 'required',
            'title' => 'required',
            'news_image' => 'image|nullable|max:1999',
            'body1' => 'required',
            'newsdate' => 'required'
        ),array(
            'category_id.required' => 'Category name is required',
            'title.required' => 'Title is required',
            'body1.required' => 'News First Paragraph is required',
            'newsdate.required' => 'News Date is required'
        ));

        $news = News::find($id);
        //Handle file upload
        if($request->hasFile('news_image')){
            //Get filemame with the extension
            Storage::delete('public/news_image'.'/'.$news->news_image);
            Storage::delete('public/news_image_large'.'/'.$news->news_image);
            Storage::delete('public/news_image_medium'.'/'.$news->news_image);
            Storage::delete('public/news_image_small'.'/'.$news->news_image);
            $filenameWithExt = $request->file('news_image')->getClientOriginalName();
            //Get hust filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just ext
            $extension = $request->file('news_image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('news_image')->storeAs('public/news_image', $fileNameToStore);

            $img = Image::make($request->file('news_image'))->resize(730, 490)->encode($extension);
            Storage::put('public/news_image_large'.'/'.$fileNameToStore,$img);
            
            $img = Image::make($request->file('news_image'))->resize(400, 270)->encode($extension);
            Storage::put('public/news_image_medium'.'/'.$fileNameToStore,$img);

            $img = Image::make($request->file('news_image'))->resize(70, 50)->encode($extension);
            Storage::put('public/news_image_small'.'/'.$fileNameToStore,$img);
        }


        
        $news->date = $request->input('newsdate');
        $news->category_id = $request->input('category_id');
        $news->title = $request->input('title');
        $news->body1 = $request->input('body1');
        $news->body2 = $request->input('body2');
        $news->media_url = $request->input('media_url');
        if($request->hasFile('news_image')){
            $news->news_image = $fileNameToStore;
        }
        $news->save();
        return redirect('admin/news')->with('success', 'News Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);

        if($news->news_image != 'noimage.jpg'){
            Storage::delete('public/news_image'.'/'.$news->news_image);
            Storage::delete('public/news_image_large/'.$news->news_image);
            Storage::delete('public/news_image_medium/'.$news->news_image);
            Storage::delete('public/news_image_small/'.$news->news_image);
        }
        $news->delete();
        return redirect('admin/news')->with('error', 'News Deleted');
    }

    public function search()
    {
        $search = \Request::get('search'); 

        $news = News::where('title','like','%'.$search.'%')->orderBy('id', 'DESC')->paginate(10);
        $items= Category::all();

        return view('admin/news')->with('items', $items)->with('news', $news);
    }
}
