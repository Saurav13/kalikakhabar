<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Advertise;

class AdvertiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {   
        $advertises = Advertise::orderBy('id', 'DESC')->get();
        return view('admin/advertise')->with('advertises', $advertises);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate(array(
            'adv_type' => 'required',
            'adv_image' => 'required'
        ),array(
            'adv_type.required' => 'Advertise postion is required',
            'adv_image.required' => 'Advertise Image is required'
           
        ));

        
               
        //Handle file upload
        if($request->hasFile('adv_image')){
            //Get filemame with the extension
            $filenameWithExt = $request->file('adv_image')->getClientOriginalName();
            //Get hust filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just ext
            $extension = $request->file('adv_image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('adv_image')->storeAs('public/adv_image', $fileNameToStore);
        }else{
            $fileNameToStore = 'noimage.jpg';
        }

        //create news
        $adv = new Advertise;
        $adv->adv_type = $request->input('adv_type');
        $adv->adv_image = $fileNameToStore;
        $adv->save();

        return redirect('/admin/advertise')->with('success', 'Advertise Image Filled');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adv = Advertise::find($id);
              //Handle file upload
        if($request->hasFile('adv_image')){
            //Get filemame with the extension
            $filenameWithExt = $request->file('adv_image')->getClientOriginalName();
            //Get hust filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just ext
            $extension = $request->file('adv_image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('adv_image')->storeAs('public/adv_image', $fileNameToStore);
            $adv->adv_image = $fileNameToStore;
        }


        $adv->adv_type = $request->input('adv_type');
        $adv->save();
        return redirect('admin/advertise')->with('success', 'Advertise Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertise = Advertise::find($id);

        if(!$advertise->adv_image){
            Storage::delete('public/adv_image/'.$advertise->adv_image);
        }

        $advertise->delete();
        return redirect('admin/advertise')->with('error', 'Advertise Image Deleted');
    }
}
