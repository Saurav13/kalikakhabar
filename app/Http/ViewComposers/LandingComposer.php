<?php

namespace App\Http\ViewComposers;
use Illuminate\View\View;
use App\Category;
use App\Advertise;
use App\News;

class LandingComposer
{
    public function create(View $view){
        $adv_image=Advertise::where('adv_type','top')->first();

    
        $newscollection = News::all();
        
        $slice1 = $newscollection->slice(0,1);
        $slice2= $newscollection->slice(2,3);
        $slice3 = $newscollection->slice(5,3);

        // $cat = Category::all()->first();
        // dd($cat->news);

        $midadv=Advertise::where('adv_type','middle')->get();
        $view->with('category', Category::has('news')->get())->with('adv_image', $adv_image)->with('news1', $slice1->all())
        ->with('news2', $slice2->all())->with('news3', $slice3->all())->with('advertise',Advertise::all())->with('midadv',$midadv);

    }
}