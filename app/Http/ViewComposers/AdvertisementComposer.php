<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Advertise;

class AdvertisementComposer
{
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('advertisements', Advertise::all());
    }
}