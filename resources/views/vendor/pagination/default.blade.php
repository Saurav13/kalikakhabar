@if ($paginator->hasPages())
    <ul class="pagination list-inline">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="list-inline-item disabled"><span class="u-pagination-v1__item u-pagination-v1-3 g-pa-4-13">&laquo;</span></li>
        @else
            <li class="list-inline-item"><a class="u-pagination-v1__item u-pagination-v1-3 g-pa-4-11" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="list-inline-item disabled"><span class="u-pagination-v1__item u-pagination-v1-3 g-pa-4-11">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="list-inline-item"><span class="u-pagination-v1__item u-pagination-v1-3 u-pagination-v1-3--active g-pa-4-11">{{ $page }}</span></li>
                    @else
                        <li class="list-inline-item"><a class="u-pagination-v1__item u-pagination-v1-3 g-pa-4-11" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="list-inline-item"><a class="u-pagination-v1__item u-pagination-v1-3 g-pa-4-11" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            <li class="list-inline-item disabled"><span class="u-pagination-v1__item u-pagination-v1-3 g-pa-4-11" >&raquo;</span></li>
        @endif
    </ul>
@endif
