<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Categories</h2>
  <ul class="list-group">
        @if(count($items) > 0 )
            @foreach($items as $item)
               
                <li class="list-group-item">{{$item->name}}</li>
    
             @endforeach
                    

             @else
                <h2>No Categories Found !!!</h2>
  
        @endif
  </ul>
</div>

</body>
</html>
