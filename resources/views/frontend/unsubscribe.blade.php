@extends('layouts.app')
@section('title',' | Unsubscribe')
@section('body')
<div class="container">
    <br>

    <div class="g-mb-60">
        <div class="u-heading-v3-1 g-mb-30">
          <h2 class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">Unsubscribe?</h2>
        </div>

        <form method="POST" action="{{route('unsubscribed',$token)}}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 form-group g-mb-30">
                    <p class="text-capitalize">You will no longer be updated.</p>
                </div>
            </div>

            <a href="{{ route('home') }}" class="btn btn-md u-btn-outline-teal g-font-size-12 text-uppercase g-px-25 g-py-13">Cancel</a>
            <button class="btn btn-md u-btn-outline-brown g-font-size-12 text-uppercase g-px-25 g-py-13" type="submit">Unsubscribe</button>
                
        </form>
    </div>
</div>

@stop