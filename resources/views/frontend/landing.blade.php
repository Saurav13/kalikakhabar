@extends('layouts.app')
@section('title',' | Home')
@section('body')
  <style>
      @media(min-width:200px){
        .ytb{
          height:240px;
          width:350px;
        }
      }
    @media(min-width:400px){
      .ytb{
        height:240px;
        width:350px;
      }
    }
    @media(min-width:700px){
      .ytb{
        height:400px;
        width:600px;
      }
    }
    @media(min-width:1000px){
      .ytb{
        height:490px;
        width:730px;
      }
    }
  </style>

      <!-- End Header -->

      <!-- Revolution Slider -->
      <div class="u-rev-slider-v2 g-overflow-hidden">
        <div id="revSliderWrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin: 0 auto; background: #fff; padding: 0; margin-top: 0;margin-bottom: 0;"
           data-alias="carousel-gallery"
           data-source="gallery">
          <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
          <div id="revSlider" class="rev_slider fullwidthabanner" style="display: none;"
               data-version="5.4.1">
            <ul>
              <!-- SLIDE  -->
              @foreach($categories as $c)
                @if($c->news->count()>0)
                  <li data-index="rs-4{{$c->id}}"
                      data-transition="fade"
                      data-slotamount="default"
                      data-hideafterloop="0"
                      data-hideslideonmobile="off"
                      data-easein="default"
                      data-easeout="default"
                      data-masterspeed="300"
                      data-thumb="{{ asset('storage/news_image/'.$c->news()->orderBy('id','DESC')->first()->news_image) }}"
                      data-rotate="0"
                      data-saveperformance="off"
                      data-param1="{{ $c->name }} : "
                      data-title="{{ $c->news()->orderBy('id','DESC')->first()->title }}">
                    <!-- MAIN IMAGE -->
                    <img class="rev-slidebg" src="{{ asset('storage/news_image/'.$c->news()->orderBy('id','DESC')->first()->news_image) }}" alt="Image description"
                        data-lazyload="{{ asset('storage/news_image/'.$c->news()->orderBy('id','DESC')->first()->news_image) }}"
                        data-bgposition="center center"
                        data-kenburns="on"
                        data-duration="30000"
                        data-ease="Linear.easeNone"
                        data-scalestart="100"
                        data-scaleend="120"
                        data-rotatestart="0"
                        data-rotateend="0"
                        data-blurstart="0"
                        data-blurend="0"
                        data-offsetstart="0 0"
                        data-offsetend="0 0">
                    @if($c->news()->orderBy('id','DESC')->first()->media_url)
                    <div class="rs-background-video-layer"
                        data-forcerewind="on"
                        data-volume="100"
                        data-ytid="{{array_slice(explode('/',$c->news()->orderBy('id','DESC')->first()->media_url),-1)[0]}}"
                        data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&amp;hd=1&amp;wmode=opaque&amp;showinfo=0&amp;rel=0;"
                        data-videorate="1"
                        data-videowidth="100%"
                        data-videoheight="100%"
                        data-videocontrols="none"
                        data-videoloop="none"
                        data-forceCover="1"
                        data-aspectratio="16:9"
                        data-autoplay="true"
                        data-autoplayonlyfirsttime="false"
                        data-nextslideatend="true">
                    </div>
                    @endif
                    <!-- LAYER NR. 1 -->
                    <div id="slide-41-layer-1" class="tp-caption tp-resizeme" style="z-index: 7; min-width: 600px; max-width: 600px; white-space: normal; font-size: 13px; line-height: 30px; font-weight: 700; color: #6281c8;"
                        data-x="['center','center','center','center']"
                        data-y="['top','top','top','top']"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['155','155','145','180']"
                        data-width="['600','550','350','260']"
                        data-height="none"
                        data-whitespace="normal"
                        data-type="text"
                        data-basealign="slide"
                        data-responsive_offset="on"
                        data-frames='[
                          {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":100,"ease":"Power4.easeOut"},
                          {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                        ]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]">
                        @if($c->news()->orderBy('id','DESC')->first()->media_url)
                        @else
                        {{$c->name}} <span class="g-color-secondary-dark-v1 g-font-size-10 g-opacity-0_3 g-pos-rel g-top-minus-2 mx-2">|</span> <span class="g-color-secondary-dark-v1 g-font-weight-400 text-capital nepali">{{$c->news()->orderBy('id','desc')->first()->date}}</span>
                        @endif
                        </div>
                    @if($c->news()->orderBy('id','DESC')->first()->media_url)
                    @else
                    <!-- LAYER NR. 2 -->
                    <a id="slide-41-layer-1" class="tp-caption tp-resizeme nepali" href="#!" target="_self" style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; font-size: 40px; line-height: 55px; font-weight: 500; color: #fff; font-family: Roboto Slab; text-decoration: none;"
                      data-x="['center','center','center','center']"
                      data-y="['top','top','top','top']"
                      data-hoffset="['0','0','0','0']"
                      data-voffset="['200','200','180','220']"
                      data-fontsize="['40','40','30','25']"
                      data-lineheight="['55','55','45','35']"
                      data-width="['600','550','350','260']"
                      data-height="none"
                      data-whitespace="normal"
                      data-type="text"
                      data-actions=""
                      data-basealign="slide"
                      data-responsive_offset="on"
                      data-frames='[
                        {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":200,"ease":"Power4.easeOut"},
                        {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                      ]'
                      data-textAlign="['center','center','center','center']"
                      data-paddingtop="[0,0,0,0]"
                      data-paddingright="[0,0,0,0]"
                      data-paddingbottom="[0,0,0,0]"
                      data-paddingleft="[0,0,0,0]">
                    {{$c->news()->orderBy('id','DESC')->first()->title}}
                    </a>

                    <!-- LAYER NR. 4 -->
                    <div id="slide-41-layer-4" class="tp-caption tp-resizeme"
                        data-x="['center','center','center','center']"
                        data-y="['center','center','center','center']"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['50','50','0','0']"
                        data-fontsize="['15','15','15','13']"
                        data-lineheight="['30','30','30','25']"
                        data-width="['600','550','350','260']"
                        data-height="none"
                        data-whitespace="normal"
                        data-type="text"
                        data-basealign="slide"
                        data-responsive_offset="on"
                        data-frames='[
                          {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},
                          {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                        ]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]">
                        <a class="btn u-btn-primary g-color-white g-font-weight-700 g-font-size-13 text-uppercase rounded g-py-12 g-px-20 nepali" href="{{URL::to('/singlenews'.'/'.$c->news->first()->slug)}} ">Read More</a>
                    </div>
                    @endif
                  </li>
                @endif
              @endforeach

            </ul>
          </div>
        </div>
      </div>
      <!-- End Revolution Slider -->
      <div class="container g-pt-20">
        <div class="row">
            <div class="alert alert-danger alert-dismissible" role="alert" style="background-color:#fff; border-color:#fff;margin-bottom:0px;">
                <strong class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">Headlines</strong> <marquee><p style="font-family: Impact; font-size: 25pt;color:black;" class="nepali">
                {{$headlines}}  
                </p></marquee>
            </div>
        </div>
      </div>
      <br>
      <div class="container g-pt-5">
        <div class="row">
          <div class="col-md-6">
            <div class="card" style="height:130px; background-color:rgb(41, 46, 44)">

            </div>
          </div>
          <div class="col-md-6">
            <div class="card" style="height:130px; background-color:rgb(41, 46, 44)">

            </div>
          </div>
        </div>
      </div>
      <br>
      <!-- Popular News -->
      <div class="container g-pt-20 g-pb-500">
        

        <div class="row">
          <!-- Popular News -->
          <div class="col-md-8 g-mb-50">
            <!-- Article -->
          
            <!-- Article -->
              <!-- Article -->
              @foreach($news->slice(0,3) as $n)
              <article class="g-mb-40">
                  <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                    
                    @if($n->media_url)
                    <iframe class="ytb" src="{{$n->media_url}}"></iframe>
                    @else
                    <img class="img-fluid w-100" src="{{ asset('storage/news_image/'.$n->news_image) }}" alt="Image Description">
                    @endif                    
                    <figcaption class="g-pos-abs g-top-20 g-left-20">
                      <a class="btn btn-xs u-btn-teal text-uppercase rounded-0" href="#!">{{$n->category->name}}</a>
                    </figcaption>
                  </figure>
                  <h3 class="h4 g-mb-10">
                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="{{URL::to('singlenews'.'/'.$n->slug)}}">{{$n->title}}</a>
                  </h3>
                  <ul class="list-inline g-color-gray-dark-v4 g-font-size-12">
                    <li class="list-inline-item">
                      {{$n->date}}
                    </li>
                    {{--  <li class="list-inline-item">/</li>
                    <li class="list-inline-item">
                      <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
                        <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i> 24
                      </a>
                    </li>  --}}
                  </ul>
    
                  <p class="g-color-gray-dark-v41">{!!substr($n->body1,0,800)!!}...</p>
                  <a class="g-font-size-12" href="{{URL::to('/singlenews'.'/'.$n->slug)}}">Read More..</a>
              </article>
              @endforeach
                  <!-- Article -->
            
            
                <!-- Article -->
 
          </div>
          <!-- End Popular News -->

          <!-- Popular News -->
          <div class="col-md-4 g-mb-50">
            <!-- Article -->
            <h3>Other News</h3>
            @foreach($news->slice(3) as $n)
            <article class="media g-bg-white g-pa-10">
              <figure class="d-flex g-width-70 g-height-70 g-pos-rel mr-3">
                <img class="img-fluid" src="{{ asset('storage/news_image_small/'.$n->news_image) }}" alt="Image Description">
                @if($n->media_url)
                <figcaption class="g-absolute-centered">
                  <a class="js-fancybox d-block" href="javascript:;"
                     data-src="{{$n->media_url}}"
                     data-speed="350"
                     data-caption="Single Image">
                    <span class="u-icon-v2 u-icon-size--xs g-brd-white g-color-white g-color-primary--hover g-bg-white--hover rounded-circle g-cursor-pointer">
                      <i class="g-pos-rel g-left-2 fa fa-play"></i>
                    </span>
                  </a>
                </figcaption>
                @endif
              </figure>

              <div class="media-body"><br>
                <h4 class="g-font-size-13 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover" href="{{URL::to('/singlenews'.'/'.$n->slug)}}">{{$n->title}}</a></h4>
              </div>
            </article>
            @endforeach
            <br>
            @foreach($advertise->where('adv_type','side') as $a)
            <div class="card" style="height:230px; width:400px; background-image:url({{asset('/storage/adv_image'.'/'.$a->adv_image)}});background-size: cover;background-repeat: no-repeat;">
              
            </div>
            <br>
            @endforeach
            <div class="card" style="height:230px; width:400px; background-image:url({{asset('tempadd.jpg')}});background-size: cover;background-repeat: no-repeat;">

            </div>
            <br>
           
            <!-- End Article -->
          </div>
          <!-- End Popular News -->
        </div>

      </div>
      <!-- End Popular News -->
      @foreach($categories as $c)
      <div class="container g-pt-5">
        <div class="row">
          <div class="col-md-6">
            <div class="card" style="height:130px; background-image:url({{asset('/storage/adv_image'.'/'.$midadv->shuffle()->first()->adv_image)}});background-size: cover;background-repeat: no-repeat;">

            </div>
          </div>
          <div class="col-md-6">
            <div class="card" style="height:130px; background-image:url({{asset('/storage/adv_image'.'/'.$midadv->shuffle()->first()->adv_image)}});background-size: cover;background-repeat: no-repeat;">

            </div>
          </div>
        </div>
      </div>
      

      
      <!-- Weekly News -->
      <div class="container g-pt-100 g-pb-500">
        <div class="u-heading-v3-1 g-mb-30">
          <h2 class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">{{$c->name}}</h2>
        </div>

        <div class="row">
          <!-- Weekly News -->
          @foreach($c->news->slice(0,3) as $n)
          
          <div class="col-md-4 g-mb-50">
            <!-- Article -->
            <article class="g-mb-40">
              <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                <img class="img-fluid w-100" src="{{ asset('storage/news_image_medium/'.$n->news_image) }}" alt="Image Description">

                <figcaption class="g-pos-abs g-top-20 g-left-20">
                  <a class="btn btn-xs u-btn-pink text-uppercase rounded-0" href="#!">{{$c->name}}</a>
                </figcaption>
              </figure>

              <h3 class="h4 g-mb-10">
                <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="{{URL::to('singlenews'.'/'.$n->slug)}}">{{$n->title}}</a>
              </h3>

              <ul class="list-inline g-color-gray-dark-v4 g-font-size-12">
                <li class="list-inline-item">
                  {{$n->date}}
                </li>
                {{--  <li class="list-inline-item">/</li>
                <li class="list-inline-item">
                  <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
                    <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i> 24
                  </a>
                </li>  --}}
              </ul>

              <p class="g-color-gray-dark-v41">{!! mb_substr($n->body1,0,200) !!}...</p>
              <a class="g-font-size-20" href="{{URL::to('/singlenews'.'/'.$n->slug)}}">Read More</a>
            </article>
            <!-- Article -->
          </div>
          @endforeach
        
        </div>
      </div>
      <!-- End Weekly News -->
      @endforeach



@endsection