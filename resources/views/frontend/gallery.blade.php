@extends('layouts.app')
@section('title',' | Gallery')
@section('body')
<div class="container">
    <br>
    @if(count($album)>0)
        <ul class="nav u-nav-v5-3 g-brd-bottom--md g-brd-gray-light-v4" role="tablist" data-target="nav-5-3-default-hor-left-border-bottom" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
            @foreach($album as $al)
                <li class="nav-item">
                <a class="nav-link {{$loop->iteration==1?'active':''}}" data-toggle="tab" href="#{{$al->id}}" role="tab">{{$al->name}}</a>
                </li>
            @endforeach
        </ul>

        <div id="nav-5-3-default-hor-left-border-bottom" class="tab-content g-pt-20">
            @foreach($album as $al)
                <div class="tab-pane fade show {{$loop->iteration==1?'active':''}}" id="{{$al->id}}" role="tabpanel">
                    <div class="row">
                        @foreach($al->album_images as $a)
                            <div class="col-md-3 g-mb-30">
                                <a class="js-fancybox d-block u-block-hover" href="javascript:;" data-fancybox="lightbox-gallery--03" data-src="/gallery/{{$a->image}}" data-speed="350" data-caption="Lightbox Gallery">
                                    <img class="img-fluid u-block-hover__main--mover-right" src="/gallery/{{$a->image}}" alt="Image Description">
                                </a>
                            </div>
                        @endforeach
                        
                    </div>
                </div>
                
            @endforeach
        </div>
    @else
        <div class="row">
            <div class="col-lg-12 text-center">
                <p style="margin: 60px;font-weight:  600;font-size:  15px;">No Images Yet</p>
            </div>
        </div>
    @endif
</div>

@stop