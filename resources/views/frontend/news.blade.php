@extends('layouts.app')
@section('title',' | '.$cat_name)
@section('body')
<style>
        @media(min-width:200px){
          .ytb{
            height:240px;
            width:350px;
          }
        }
      @media(min-width:400px){
      .ytb{
        height:240px;
        width:350px;
      }
    }
    @media(min-width:700px){
      .ytb{
        height:400px;
        width:600px;
      }
    }
    @media(min-width:1000px){
      .ytb{
        height:490px;
        width:730px;
      }
    }
    </style>
    
<div class="container g-pt-20">
    <div class="row">
        <div class="alert alert-danger alert-dismissible" role="alert" style="background-color:#fff; border-color:#fff;margin-bottom:0px;">
            <strong class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">Headlines</strong> <marquee><p style="font-family: Impact; font-size: 25pt;color:black;" class="nepali">
                    {{$headlines}}      
            </p></marquee>
        </div>
    </div>
    </div>
    <div class="container g-pt-5">
    <div class="row">
        <div class="col-md-6">
        <div class="card" style="height:130px; background-image:url({{asset('/storage/adv_image'.'/'.$midadv->shuffle()->first()->adv_image)}});background-size: cover;background-repeat: no-repeat;">

        </div>
        </div>
        <div class="col-md-6">
        <div class="card" style="height:130px; background-image:url({{asset('/storage/adv_image'.'/'.$midadv->shuffle()->first()->adv_image)}});background-size: cover;background-repeat: no-repeat;">

        </div>
        </div>
    </div>
</div>
<div class="container g-pt-20 g-pb-500">
    <div class="row">
            <!-- Popular News -->
        <div class="col-md-8 g-mb-50">
            <!-- Article -->
        
            <!-- Article -->
            <!-- Article -->
            @foreach($news as $new)
            <article class="g-mb-40">
                <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                    @if($new->media_url)
                    <iframe class="ytb" src="{{$new->media_url}}"></iframe>
                    @else
                    <img class="img-fluid w-100" src="/storage/news_image/{{$new->news_image}}" alt="Image Description">
                    
                    @endif
                    <figcaption class="g-pos-abs g-top-20 g-left-20">
                    <a class="btn btn-xs u-btn-teal text-uppercase rounded-0" href="#!">{{$cat_name}}</a>
                    </figcaption>
                </figure>
                <h3 class="h4 g-mb-10">
                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="/singlenews/{{$new->slug}}">{{$new->title}}</a>
                </h3>
                <ul class="list-inline g-color-gray-dark-v4 g-font-size-12">
                   <li class="list-inline-item">
                            {{$new->date}}
                    </li>
                    {{--  <li class="list-inline-item">/</li>
                    <li class="list-inline-item">
                    <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
                        <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i> 24
                    </a>
                    </li>  --}}
                </ul>

                <p class="g-color-gray-dark-v41">{!! $new->body1 !!}</p>
                <a class="g-font-size-12" href="{{URL::to('singlenews'.'/'.$new->slug)}}">Read More..</a>
                </article>
                <!-- Article -->
                @if($loop->index%2==0)
                <div class="row">
                    <div class="col-md-12">
                    <div class="card" style="height:130px; background-color:rgb(41, 46, 44)">
            
                    </div>
                    </div>
            
                </div>
                <br>
                @endif
                @endforeach
              
                <nav class="text-center" aria-label="Page Navigation">    
                
                    {!!$news->links()!!}
                </nav>

        </div>
        <!-- End Popular News -->

        <!-- Popular News -->
        <div class="col-md-4 g-mb-50">
            <!-- Article -->
            <h3>Other News</h3>
            @foreach($category as $c)
            <article class="media g-bg-white g-pa-10">
                <figure class="d-flex g-width-70 g-height-70 g-pos-rel mr-3">
                    <img class="img-fluid" src="{{asset('storage/news_image_small'.'/'.$c->news->first()->news_image)}}" alt="Image Description">
                    @if($c->news->first()->media_url)
                    <figcaption class="g-absolute-centered">
                            <a class="js-fancybox d-block" href="javascript:;"
                                data-src="{{$c->news->first()->media_url}}"
                                data-speed="350"
                                data-caption="Single Image">
                                <span class="u-icon-v2 u-icon-size--xs g-brd-white g-color-white g-color-primary--hover g-bg-white--hover rounded-circle g-cursor-pointer">
                                <i class="g-pos-rel g-left-2 fa fa-play"></i>
                                </span>
                            </a>
                    </figcaption>
                    @endif
                
                </figure>

                <div class="media-body">
                <br>
                        <h4 class="g-font-size-13 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover" href="{{URL::to('/singlenews'.'/'.$c->news->first()->slug)}}">{{$c->news->first()->title}}</a></h4>
                </div>
            </article>
            @endforeach
           
          
            <br>

            @foreach($advertise->where('adv_type','side') as $a)
            <div class="card" style="height:230px; width:400px; background-image:url({{asset('/storage/adv_image'.'/'.$a->adv_image)}});background-size: cover;background-repeat: no-repeat;">
              
            </div>
            <br>
            @endforeach
                        <!-- End Article -->
        </div>
        <!-- End Popular News -->
    </div>
</div>
@endsection