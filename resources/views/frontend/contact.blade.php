@extends('layouts.app')

@section('title',' | Contact Us')

@section('body')
<div class="container">
    <br>

    <div class="g-mb-60">
        <div class="u-heading-v3-1 g-mb-30">
          <h2 class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">Leave a Message</h2>
        </div>

        <form method="POST" action="{{ route('contactSend') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 form-group g-mb-30">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-pa-15" type="text" placeholder="Your Name" name="name" required>
                </div>

                <div class="col-md-6 form-group g-mb-30">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-pa-15" type="email" placeholder="Email" name="email" required>
                </div>
            </div>

            <div class="g-mb-30">
                <textarea class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-3 g-pa-15" rows="12" placeholder="Your Message" name="message" required></textarea>
            </div>

            <button class="btn u-btn-primary g-font-size-12 text-uppercase g-px-25 g-py-13" type="submit">
                <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-2"></i> Send
            </button>
        </form>
    </div>
</div>

@stop