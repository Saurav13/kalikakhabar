<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>Everest Media @yield('title')</title></title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/main-assets/favicon.ico">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto+Slab:300,400,700%7COpen+Sans:400,600,700">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/bootstrap/bootstrap.min.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-line-pro/style.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/animate.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/fancybox/jquery.fancybox.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/revolution-slider/style.css">
    {{--  <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/fonts/nepali/pretti.css')}}">  --}}

    <!-- CSS Unify Theme -->
    <link rel="stylesheet" href="/assets/css/styles.bm-classic.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/main-assets/assets/css/custom.css">
  </head>
