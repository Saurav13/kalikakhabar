      <!-- Footer -->
      <footer class="g-bg-secondary">
     

        <div class="container">
          <!-- Footer - Content -->
          <div class="g-brd-bottom--md g-brd-secondary-light-v2 g-pb-30--md g-mb-30">
          
          </div>
          <!-- End Footer - Content -->

          <!-- Footer - Top Section -->
          <div class="g-brd-bottom g-brd-secondary-light-v2 g-mb-30">
            <div class="row align-items-center">
              <div class="col-md-4 g-hidden-sm-down g-mb-30">
                <!-- Logo -->
                <a href="bm-classic-home-2.html">
                  <img class="g-width-150" src="{{url ('assets/img/em.png')}}" alt="Everest Media">
                </a>
                <!-- End Logo -->
              </div>

              <div class="col-md-4 ml-auto g-mb-30">
                <!-- Social Icons -->
                <ul class="list-inline mb-0">
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 u-icon-size--sm g-brd-secondary-light-v2 g-color-secondary-dark-v2 g-color-white--hover g-bg-primary--hover g-font-size-default rounded" href="https://www.facebook.com/Kalika-Khabar-1586308464786509/"
                      target="_blank"
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Like Us on Facebook">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 u-icon-size--sm g-brd-secondary-light-v2 g-color-secondary-dark-v2 g-color-white--hover g-bg-primary--hover g-font-size-default rounded" href="https://twitter.com/kalikakhabar11"
                      target="_blank" 
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Follow Us on Twitter">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 u-icon-size--sm g-brd-secondary-light-v2 g-color-secondary-dark-v2 g-color-white--hover g-bg-primary--hover g-font-size-default rounded"
                      style="cursor:  pointer;"
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Call Us on 9851185703">
                      <i class="fa fa-phone"></i>
                    </a>
                  </li>
                </ul>
                <!-- End Social Icons -->
              </div>
              <div class="col-md-4 text-center text-md-right g-mb-30">
                <!-- Subscribe Form -->
                <form class="input-group rounded" action="{{ route('subscribe') }}" method="POST">
                  {{ csrf_field() }}
                  <input class="form-control g-brd-secondary-light-v2 g-color-secondary-dark-v1 g-placeholder-secondary-dark-v1 g-bg-secondary-light-v3 g-font-weight-400 g-font-size-13 rounded g-px-20 g-py-12" type="email" name="email" placeholder="Enter your email address" required>
                  <span class="input-group-addon g-brd-none g-py-0 g-pr-0">
                    <button class="btn u-btn-white g-color-primary--hover g-font-weight-600 g-font-size-13 text-uppercase rounded g-py-12 g-px-20" type="submit">Subscribe</button>
                  </span>
                </form>
                <!-- End Subscribe Form -->
              </div>

              
            </div>
          </div>
          <!-- End Footer - Top Section -->

          <!-- Footer - Bottom Section -->
          <div class="row align-items-center">
            <div class="col-md-2 g-brd-right--md g-brd-secondary-light-v2 g-mb-30">
              <!-- Copyright -->
              <p class="g-color-secondary-light-v1 g-font-size-12 mb-0">&copy; 2018 Everest Media</p>
              <!-- End Copyright -->
            </div>

            <div class="col-md-8 g-brd-right--md g-brd-secondary-light-v2 g-mb-30">
              <!-- Links -->
              <ul class="list-inline mb-0">
                <li class="list-inline-item g-pl-0 g-pr-10">
                  <a class="u-link-v5 g-color-secondary-light-v1 g-font-size-12" href="{{ route('home') }}">Home</a>
                </li>
                <li class="list-inline-item g-px-10">
                  <a class="u-link-v5 g-color-secondary-light-v1 g-font-size-12" href="{{ URL::to('galleries') }}">Gallery</a>
                </li>
                <li class="list-inline-item g-px-10">
                  <a class="u-link-v5 g-color-secondary-light-v1 g-font-size-12" href="{{ URL::to('contact-us') }}">Contact Us</a>
                </li>
                
              </ul>
              <!-- End Links -->
            </div>

            <div class="col-md-2 g-brd-right--md g-brd-secondary-light-v2 g-mb-30">
              <!-- Copyright -->
              <p class="g-color-secondary-light-v1 g-font-size-12 mb-0"><strong>Powered by inCube</strong></p>
              <!-- End Copyright -->
            </div>

          </div>
          <!-- End Footer - Bottom Section -->
        </div>
      </footer>
      <!-- End Footer -->

      <!-- Go To -->
      <a class="js-go-to u-go-to-v2" href="#!"
         data-type="fixed"
         data-position='{
           "bottom": 15,
           "right": 15
         }'
         data-offset-top="400"
         data-compensation="#js-header"
         data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
      <!-- End Go To -->
    </main>

    <div class="u-outer-spaces-helper"></div>

    <!-- JS Global Compulsory -->
    <script src="/main-assets/assets/vendor/jquery/jquery.min.js"></script>
    <script src="/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
    <script src="/main-assets/assets/vendor/popper.min.js"></script>
    <script src="/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- JS Implementing Plugins -->
    <script src="/main-assets/assets/vendor/appear.js"></script>
    <script src="/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
    <script src="/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="/main-assets/assets/vendor/fancybox/jquery.fancybox.min.js"></script>

    <!-- JS Unify -->
    <script src="/main-assets/assets/js/hs.core.js"></script>
    <script src="/main-assets/assets/js/components/hs.header.js"></script>
    <script src="/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="/main-assets/assets/js/components/hs.dropdown.js"></script>
    <script src="/main-assets/assets/js/components/hs.carousel.js"></script>
    <script src="/main-assets/assets/js/components/hs.go-to.js"></script>
    <script src="/main-assets/assets/js/components/hs.popup.js"></script>

    <!-- JS Revolution Slider -->
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>

    <!-- JS Customization -->
    <script src="/main-assets/assets/js/custom.js"></script>
    @yield('js')
    <!-- JS Plugins Init. -->
    <script>
      $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of MegaMenu
        $('.js-mega-menu').HSMegaMenu();

        // initialization of HSDropdown component
        $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
          afterOpen: function () {
            $(this).find('input[type="search"]').focus();
          }
        });

        // initialization of carousel
        $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

        // initialization of popups
        $.HSCore.components.HSPopup.init('.js-fancybox');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
      });

      // Revolution Slider
      var tpj = jQuery,
        revAPI;

      tpj(document).ready(function () {
        if (tpj('#revSlider').revolution == undefined) {
          revslider_showDoubleJqueryError('#revSlider');
        } else {
          revAPI = tpj('#revSlider').show().revolution({
            sliderType: 'carousel',
            jsFileLocation: 'revolution/js/',
            sliderLayout: 'fullscreen',
            dottedOverlay: 'none',
            delay: 9000,
            navigation: {
              keyboardNavigation: 'off',
              keyboard_direction: 'horizontal',
              mouseScrollNavigation: 'off',
              mouseScrollReverse: 'default',
              onHoverStop: 'on',
              thumbnails: {
                style: 'gyges',
                enable: true,
                width: 50,
                height: 50,
                min_width: 50,
                wrapper_padding: 30,
                wrapper_color: 'transparent',
                tmp: '<span class="tp-thumb-img-wrap">' +
                '<span class="tp-thumb-image"></span>' +
                '</span>',
                visibleAmount: 5,
                hide_onmobile: false,
                hide_over: 1240,
                hide_onleave: false,
                direction: 'horizontal',
                span: true,
                position: "outer-bottom",
                space: 0,
                h_align: 'center',
                v_align: 'bottom',
                h_offset: 0,
                v_offset: 0
              },
              tabs: {
                style: 'gyges',
                enable: true,
                width: 303,
                height: 110,
                min_width: 310,
                wrapper_padding: 5,
                wrapper_color: "#f2f4f8",
                wrapper_opacity: "1",
                tmp: '<div class="tp-tab-content">' +
                '<span class="tp-tab-date">@{{param1}}</span>' +
                '<span class="tp-tab-title">@{{title}}</span>' +
                '</div>' +
                '<div class="tp-tab-image"></div>',
                visibleAmount: 7,
                hide_onmobile: true,
                hide_onleave: false,
                hide_delay: 200,
                direction: 'horizontal',
                span: true,
                position: "inner",
                space: 5,
                h_align: "center",
                v_align: "bottom",
                h_offset: 0,
                v_offset: 0
              }
            },
            carousel: {
              horizontal_align: 'center',
              vertical_align: 'center',
              fadeout: 'off',
              maxVisibleItems: 1,
              infinity: 'on',
              space: 5,
              stretch: 'on',
              showLayersAllTime: 'off',
              easing: 'Power3.easeInOut',
              speed: '800'
            },
            responsiveLevels: [1240, 1024, 778, 480],
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: [800, 700, 400, 300],
            gridheight: [600, 600, 500, 400],
            lazyType: 'single',
            shadow: 0,
            spinner: 'off',
            stopLoop: 'off',
            stopAfterLoops: 0,
            stopAtSlide: 1,
            shuffle: 'off',
            autoHeight: 'off',
            hideThumbsOnMobile: 'off',
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
              simplifyAll: 'off',
              nextSlideOnWindowFocus: 'off',
              disableFocusListener: false,
            }
          });
        }
      });
    </script>
  </body>
</html>
