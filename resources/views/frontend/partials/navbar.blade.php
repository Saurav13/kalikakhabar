
  <body>
    <main>
      <style>
        .topadv{

        }
      </style>
      <!-- Header -->
      <header id="js-header" class="u-header u-header--static u-shadow-v19">
        <!-- Top Bar -->
        <div class="u-header__section g-brd-bottom g-brd-gray-light-v4 g-py-18">
          <div class="container">
            <div class="row align-items-center">
              <!-- Logo -->
              <div class="col-md-3 g-hidden-md-down">
                <a href="/" class="navbar-brand">
                  <img height="120" width="180" src="{{url ('assets/img/em.png')}}" alt="Everest Media">
                </a>
              </div>
              <!-- End Logo -->
              <div class="col-9 col-md-9 pull-right">
                <div class="card">

                
                  
                  <img class="img-fluid" style="max-height:100px; max-width:800px;"src="/storage/adv_image/{{$adv_image->adv_image}}">
                
            

                </div>
              </div>

            

            </div>
          </div>
        </div>
        <!-- End Top Bar -->

        <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
          <nav class="js-mega-menu navbar navbar-expand-lg g-px-0">
            <div class="container g-px-15">
              <!-- Logo -->
              <a class="navbar-brand g-hidden-lg-up" href="bm-classic-home-page-1.html">
                <img src="{{url ('assets/img/em.png')}}" height="75px" width="135px" alt="Logo">
              </a>
              <!-- End Logo -->

              <!-- Responsive Toggle Button -->
              <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 ml-auto" type="button"
                      aria-label="Toggle navigation"
                      aria-expanded="false"
                      aria-controls="navBar"
                      data-toggle="collapse"
                      data-target="#navBar">
                <span class="hamburger hamburger--slider g-pa-0">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->

              <!-- Navigation -->
              <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg" id="navBar">
                <ul class="navbar-nav g-font-weight-600">
                  <!-- Home - Submenu -->
                  <li class="nav-item g-mr-10--lg g-mr-20--xl">
                    <a id="nav-link--home" class="nav-link text-uppercase g-color-primary--hover g-px-0" href="/"
                       aria-haspopup="true"
                       aria-expanded="false"
                       aria-controls="nav-submenu--home">
                      
                      Home
                    </a>

                  
                  </li>

                  <!-- Pages - Submenu -->
                  
                  <!-- End Pages - Submenu -->

                  <!-- Mega Menu Item -->

                  
              @foreach($category as $cat)
        

                  <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-20--xl"
                      data-animation-in="fadeIn"
                      data-animation-out="fadeOut"
                      data-position="right">
                    <a id="mega-menu-label-1" class="nav-link text-uppercase g-px-0" href="/news/{{$cat->name}}" aria-haspopup="true" aria-expanded="false">{{$cat->name}}
                      <i class="hs-icon hs-icon-arrow-bottom g-font-size-11 g-ml-5"></i></a>

                    <!-- Mega Menu -->
                    <div class="hs-mega-menu u-shadow-v11 g-text-transform-none g-font-weight-400 g-brd-top g-brd-primary g-brd-top-2 g-bg-white g-left-15 g-right-15 g-pa-30 g-mt-17 g-mt-7--lg--scrolling" aria-labelledby="mega-menu-label-3">
                        <div class="row">
                           
                          <div class="col-lg-4">
               
                            
                            <article>
                              <figure class="g-pos-rel">
                                <img class="img-fluid w-100 g-mb-20" src="/storage/news_image/{{$cat->news->first()->news_image }}" alt="Image Description">
                              </figure>
                              <h3 class="h4 g-mb-10"><a class="g-color-gray-dark-v2" href="/singlenews/{{$cat->news->first()->slug}}">{{$cat->news->first()->title}}</a>
                              </h3>
                              <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                              
                                <li class="list-inline-item">{{$cat->news->first()->date}}</li>
                              </ul>
                            </article>
                           
                          </div>

                        
                          <div class="col-lg-4">
                            @foreach($cat->news->slice(1,3) as $new)

                            <article class="media g-mb-20">
                  
                              <a class="d-flex mr-3" href="#!"><img class="g-width-120 g-height-70" src="/storage/news_image/{{$new->news_image}}" alt="Image Description"></a>
                              <div class="media-body">
                                <h3 class="h6">
                                  <a class="g-color-main" href="/singlenews/{{$new->slug}}">{{$new->title}}</a>
                                </h3>
                                <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                                  <li class="list-inline-item">{{$new->date}}</li>
                                  {{--  <li class="list-inline-item">/</li>
                                  <li class="list-inline-item">
                                    <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#!">
                                      <i class="fa fa-comments-o"></i> 24
                                    </a>
                                  </li>  --}}
                                </ul>
                              </div>
                            </article>

                            @endforeach
                           
                          </div>
                          <div class="col-lg-4">
                              @foreach($cat->news->slice(4,3) as $new)
  
                              <article class="media g-mb-20">
                    
                                <a class="d-flex mr-3" href="#!"><img class="g-width-120 g-height-70" src="/storage/news_image/{{$new->news_image}}" alt="Image Description"></a>
                                <div class="media-body">
                                  <h3 class="h6">
                                    <a class="g-color-main" href="/singlenews/{{$new->slug}}">{{$new->title}}</a>
                                  </h3>
                                  <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                                    <li class="list-inline-item">{{$new->date}}</li>
                                    {{--  <li class="list-inline-item">/</li>
                                    <li class="list-inline-item">
                                      <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#!">
                                        <i class="fa fa-comments-o"></i> 24
                                      </a>
                                    </li>  --}}
                                  </ul>
                                </div>
                              </article>
          
  
                              @endforeach
                            </div>
                          </div>
                        <a href="{{URL::to('/news'.'/'.$cat->name)}}" class="pull-right">View all</a>

                    <!-- End Mega Menu -->
                  </li>
                  <!-- End Mega Menu Item -->

                @endforeach

                <li class="nav-item g-mx-10--lg g-mx-20--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-position="right">
                  <a id="mega-menu-label-4" class="nav-link text-uppercase g-px-0" href="/galleries" aria-haspopup="true" aria-expanded="false">Gallery
                </a>

                <li  class="nav-item g-mx-10--lg g-mx-20--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-position="right">
                  <a class="nav-link text-uppercase g-px-0" aria-haspopup="true" aria-expanded="false" href="{{ URL::to('contact-us') }}">Contact Us</a>
                </li>

              </li>
                  <!-- End Mega Menu Item -->

                  <!-- Mega Menu Item -->
                
                  <!-- End Mega Menu Item -->

                  <!-- Mega Menu Item -->
                 
                  <!-- End Mega Menu Item -->

                  <!-- Main -->
                
                  <!-- End Main -->
                </ul>
              </div>
              <!-- End Navigation -->
            </div>
          </nav>
        </div>
      </header>
      <!-- End Header -->
      
      @if (Session::has('success'))
        <div class="row">
          <div class="col-lg-12">
            <div class="alert alert-dismissible fade show g-bg-teal-opacity-0_1 g-color-teal rounded-0" role="alert">
              <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
        
              <div class="media">
                <span class="d-flex g-mr-10 g-mt-5">
                  <i class="icon-check g-font-size-25"></i>
                </span>
                <span class="media-body align-self-center">
                  <strong>Success!</strong> {!! Session::get('success') !!}
                </span>
              </div>
            </div>
          </div>
        </div>
      @endif
      @if (Session::has('error'))
        <div class="row">
          <div class="col-lg-12">
            <div class="alert alert-dismissible fade show g-bg-red-opacity-0_1 g-color-lightred rounded-0" role="alert">
              <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
        
              <div class="media">
                <span class="d-flex g-mr-10 g-mt-5">
                  <i class="icon-check g-font-size-25"></i>
                </span>
                <span class="media-body align-self-center">
                  <strong>Error!</strong> {!! Session::get('error') !!}
                </span>
              </div>
            </div>
          </div>
        </div>
      @endif