@extends('layouts.app')

@section('title',' | '.$news->title)
@section('body')
<style>
    @media(min-width:200px){
      .ytb{
        height:240px;
        width:350px;
      }
    }
  @media(min-width:400px){
  .ytb{
    height:240px;
    width:350px;
  }
}
@media(min-width:700px){
  .ytb{
    height:400px;
    width:600px;
  }
}
@media(min-width:1000px){
  .ytb{
    height:490px;
    width:730px;
  }
}
</style>

   <!-- Page Title -->
   <section class="g-bg-secondary g-py-50" hidden>
        <div class="container">
          <div class="d-sm-flex text-center">
            <div class="align-self-center">
              <h2 class="h3 g-mb-10 g-mb-0--md">Blog Single Page</h2>
            </div>

            <div class="align-self-center ml-auto">
              <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                  <a class="u-link-v5 g-color-main" href="#!">Home</a>
                  <i class="g-color-gray-light-v2 g-ml-5">/</i>
                </li>
                <li class="list-inline-item g-mr-5">
                  <a class="u-link-v5 g-color-main" href="#!">Pages</a>
                  <i class="g-color-gray-light-v2 g-ml-5">/</i>
                </li>
                <li class="list-inline-item g-color-primary">
                  <span>Blog Single Page</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <!-- End Page Title -->

      <!-- News Content -->
      <section class="g-pt-50 g-pb-100">
        <div class="container">
          <div class="row">
            <!-- Articles Content -->
            <div class="col-lg-8 g-mb-50 g-mb-0--lg">
              <article class="g-mb-60">
                <header class="g-mb-30">
                  <h2 class="h1 g-mb-15">{{$news->title}}</h2>

                  <ul class="list-inline d-sm-flex g-color-gray-dark-v4 mb-0">
                   
                    <li class="list-inline-item">
                      {{$news->date}}
                    </li>
                    <li class="list-inline-item ml-auto">
                      <i class="icon-eye u-line-icon-pro align-middle mr-1"></i> Views {{ Counter::showAndCount('news',$news->id) }}
                    </li>
                  </ul>

                  <hr class="g-brd-gray-light-v4 g-my-15">

                  <ul class="list-inline text-uppercase mb-0">
                      <li class="list-inline-item g-mr-10">
                          <a target="_blank" class="btn u-btn-facebook g-font-size-12 rounded g-px-20--sm g-py-10 social-share" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}">
                            <i class="fa fa-facebook g-mr-5--sm"></i> <span class="g-hidden-xs-down">Share on Facebook</span>
                          </a>
                          <a target="_blank" class="btn u-btn-twitter g-font-size-12 rounded g-px-20--sm g-py-10 social-share" href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}">
                            <i class="fa fa-twitter g-mr-5--sm"></i> <span class="g-hidden-xs-down">Share on Twitter</span>
                          </a>
                          <a target="_blank" class="btn u-btn-red g-font-size-12 rounded g-px-20--sm g-py-10 social-share" href="https://plus.google.com/share?url={{ urlencode(Request::fullUrl()) }}">
                            <i class="fa fa-google-plus g-mr-5--sm"></i> <span class="g-hidden-xs-down">Share on Google</span>
                          </a>
                    </li>
                  </ul>
                </header>

                <div class="g-font-size-16 g-line-height-1_8 g-mb-30">
                  <figure class="u-shadow-v25 g-mb-30">
                      @if($news->media_url)
                      <iframe class="ytb" src="{{$news->media_url}}"></iframe>
                      @endif
                    <img class="img-fluid w-100" src="/storage/news_image_large/{{$news->news_image}}" alt="Image Description">
                  </figure>
                 <style>
                   .darkimp p,h1,h2,h3,h4,h5,h6,b{
                    color: #333 !important;
                   }
                 </style>
                  <div class="darkimp" style="text-align:justify"> {!!$news->body1!!}
                 <div>
                <div class="container g-pt-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card" style="height:130px; background-color:rgb(41, 46, 44)">
                
                            </div>
                        </div>
                       
                    </div>
                </div>
                <br>
                <siv class="darkimp" style="text-align:justify">
                {!!$news->body2!!}
                </div>
                 </div>


                <hr class="g-brd-gray-light-v4">

                <!-- Social Shares -->
                <div class="g-mb-30 social">
                  <ul class="list-inline text-uppercase mb-0">
                   
                    <li class="list-inline-item g-mr-10">
                      <a target="_blank" class="btn u-btn-facebook g-font-size-12 rounded g-px-20--sm g-py-10 social-share" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}">
                        <i class="fa fa-facebook g-mr-5--sm"></i> <span class="g-hidden-xs-down">Share on Facebook</span>
                      </a>
                      <a target="_blank" class="btn u-btn-twitter g-font-size-12 rounded g-px-20--sm g-py-10 social-share" href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}">
                        <i class="fa fa-twitter g-mr-5--sm"></i> <span class="g-hidden-xs-down">Share on Twitter</span>
                      </a>
                      <a target="_blank" class="btn u-btn-red g-font-size-12 rounded g-px-20--sm g-py-10 social-share" href="https://plus.google.com/share?url={{ urlencode(Request::fullUrl()) }}">
                        <i class="fa fa-google-plus g-mr-5--sm"></i> <span class="g-hidden-xs-down">Share on Google</span>
                      </a>
                     
                    </li>
                  </ul>
                </div>
                <!-- End Social Shares -->

                <hr class="g-brd-gray-light-v4 g-mb-40">

                <!-- Related Articles -->
                <div class="g-mb-40">
                  <div class="u-heading-v3-1 g-mb-30">
                    <h2 class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">Related News</h2>
                  </div>

                  <div class="row">
                    <!-- Article Video -->
                    @foreach($category->where('id',$news->category_id)->first()->news->slice(0,6) as $n)
                    <div class="col-lg-4 col-sm-6 g-mb-30">
                      <article>
                        <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                          <img class="img-fluid w-100" src="/storage/news_image_medium/{{$n->news_image}}" alt="Image Description">
                        </figure>

                        <h3 class="g-font-size-16 g-mb-10">
                          <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="{{URL::to('/singlenews'.'/'.$n->slug)}}">{{$n->title}}</a>
                        </h3>
                      </article>
                    </div>
                    @endforeach
                    <!-- End Article Video -->
                  </div>
                </div>

        
             
            
              </article>

              <div id="stickyblock-end"></div>
            </div>
            <!-- End Articles Content -->

            <!-- Sidebar -->
            <div class="col-lg-4">
              <!-- Recent Posts -->
              <div class="g-mb-30">
                <div class="u-heading-v3-1 g-mb-30">
                  <h2 class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">Recent News</h2>
                </div>
                @foreach($category as $c)
                <!-- Article -->
                <article class="media g-mb-30">
                  <a class="d-flex u-shadow-v25 mr-3" href="#!">
                    <img class="g-width-60 g-height-60" src="{{asset('storage/news_image_small'.'/'.$c->news->first()->news_image)}}" alt="Image Description">
                  </a>

                  <div class="media-body">
                    <h3 class="h6">
                      <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#!">{{$c->news->first()->title}}</a>
                    </h3>

                    <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                      <li class="list-inline-item">
                        {{$c->news->first()->date}}
                      </li>
                      
                    </ul>
                  </div>
                </article>
                @endforeach
                <!-- End Article -->
              </div>
              <!-- End Recent Posts -->

              <div id="stickyblock-start" class="js-sticky-block g-sticky-block--lg g-pt-20" data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">
                <!-- News Feed -->
               
                @foreach($advertise->where('adv_type','side') as $a)
                <div class="card" style="height:230px; width:400px; background-image:url({{asset('/storage/adv_image'.'/'.$a->adv_image)}});background-size: cover;background-repeat: no-repeat;">

                </div>
                <br>
                @endforeach

              </div>
            </div>
            <!-- End Sidebar -->
          </div>
        </div>
      </section>
      <!-- End News Content -->

@endsection 


@section('js')
<script>
    var popupMeta = {
      width: 400,
      height: 400
  }
  $('.social').on('click', '.social-share', function(event){
      event.preventDefault();
  
      var vPosition = Math.floor(($(window).width() - popupMeta.width) / 2),
          hPosition = Math.floor(($(window).height() - popupMeta.height) / 2);
      
      var url = $(this).attr('href');
      var popup = window.open(url, 'Social Share',
          'width='+popupMeta.width+',height='+popupMeta.height+
          ',left='+vPosition+',top='+hPosition+
          ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');
  
      if (popup) {
          popup.focus();
          return false;
      }
  });
  </script>
@endsection