@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Dashboard</h2>
        </div>
      
        <div class="container">
                <section id="minimal-statistics-bg">
                        <div class="row">
                            <div class="col-xs-12 mt-1 mb-3">
                                <h4>Kalika Statics </h4>
                                <p>Statistics on minimal cards.</p>
                                <hr>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-xl-3 col-lg-6 col-xs-12">
                                <div class="card bg-cyan">
                                    <div class="card-body">
                                        <div class="card-block">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <i class="icon-pencil white font-large-2 float-xs-left"></i>
                                                </div>
                                                <div class="media-body white text-xs-right">
                                                    <h3>{{$newscount}}</h3>
                                                    <span>Total News</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-xs-12">
                                <div class="card bg-deep-orange">
                                    <div class="card-body">
                                        <div class="card-block">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <i class="icon-chat1 white font-large-2 float-xs-left"></i>
                                                </div>
                                                <div class="media-body white text-xs-right">
                                                    <h3>{{ $adscount }}</h3>
                                                    <span>Total Ads</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-xs-12">
                                    <div class="card bg-teal">
                                        <div class="card-body">
                                            <div class="card-block">
                                                <div class="media">
                                                    <div class="media-body white text-xs-left">
                                                        <h3>{{ Counter::allHits(1) }}</h3>
                                                        <span>Today's Visit</span>
                                                    </div>
                                                    <div class="media-right media-middle">
                                                        <i class="icon-user1 white font-large-2 float-xs-right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-xl-3 col-lg-6 col-xs-12">
                                <div class="card bg-pink">
                                    <div class="card-body">
                                        <div class="card-block">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <i class="icon-map1 white font-large-2 float-xs-left"></i>
                                                </div>
                                                <div class="media-body white text-xs-right">
                                                    <h3>{{ Counter::allHits() }}</h3>
                                                    <span>Total Visits</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                
                    </section>
            </div>
    </div>
   
 
@stop

@section('js')

@endsection