@extends('layouts.admin')

@section('body')

<style>
        input#categoryid,#editcategoryid{
            font-family:'preetinormal';
            font-size:24px;
        }
        
        </style>

@if (count($errors)>0 )
<div class="alert alert-dismissible fade in mb-2">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" style="color:black">&times;</span>
    </button>
    <ul class="list-group">
        @foreach ($errors->all() as $error)
        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
        @endforeach
    </ul>
</div>	
@endif
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Category</h2>
        </div>
        
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="row">
                        <form action="/admin/category" method="POST" enctype="multipart/form-data">    
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Add Category:</label>
                                <input type="text" class="form-control" id="categoryid" name="name" placeholder="Eg. Home, Sports...." >
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                         
                         
                                   
                        </form>

                        {{--  {!! Form::open(['action' => 'Admin\CategoryController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group">
                            {{Form::label('name', 'Name')}}
                            {{Form::text('name','', ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>  
                     
                        {{Form::submit('Submit',['class' => 'btn btn-primary'])}}       
                    {!! Form::close() !!}  --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="table-responsive">
                            <table class="table">
                                    <thead class="thead-inverse">
                                        <tr>
                            
                                            <th>Category Name</th>
                                            <th>Delete</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($items as $item)
                                        <tr>
                                         
                                            <td style="font-family:'preetinormal'">{{ $item->name }}</td>
                                            <td>
                                            
                                                <button type="button" class="btn btn-outline-danger " data-toggle="modal" data-target="#delModal{{ $item->id }}">
                                                    Delete
                                                </button>
                                                <div class="modal fade text-xs-left" id="delModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                              <div class="modal-content">
                                                                <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                                  <h4 class="modal-title" id="myModalLabel2"><i class="icon-trash"></i> Are u Sure ?</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                 <center> <h5 class="text-center">Changes cannot be undone !!!</h5>
                                                                </center>
                                                                
                                                              <br>
                                                                  
                                                                 <form action="/admin/category/{{$item->id}}" method="POST" enctype="multipart/form-data" >    
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('DELETE') }}
                                                                    
                                                                    <center> <button type="submit" class="btn btn-danger "> <i class="icon-trash"></i> Delete</button></center>
                                                                </form>
                                                                      
                                                                 
                                                                </div>
                                                                <div class="modal-footer">
                                                                     
                                                                  <button type="button" class="btn grey btn-outline-warning" data-dismiss="modal">Close</button>
                                                        
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                </td>
                
                                           
                                       
                                            <td>
                                                {{--  <a href="/admin/category/{{$item->id}}/edit" class="btn btn-outline-warning">Edit</a>  --}}
                                                <button type="button" class="btn btn-outline-warning " data-toggle="modal" data-target="#editModal{{ $item->id }}">
                                                        Edit
                                                    </button>
                                                    <div class="modal fade text-xs-left" id="editModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                              <div class="modal-content">
                                                                <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                                  <h4 class="modal-title" id="myModalLabel2"><i class="icon-edit"></i> Edit Category </h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                  <h5 class="">Rename Category</h5>
                                                        
                                                                       
                                                                <form action="/admin/category/{{$item->id}}" method="POST" enctype="multipart/form-data">    
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('PUT') }}
                                                                    <div class="form-group">
                                                                        
                                                                        <input type="text" class="form-control" name="name" id="editcategoryid" value="{{$item->name}}">
                                                                    </div>

                                                                
                                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                            
                                                                
                                                                        
                                                                </form>
                                                                 
                                                                </div>
                                                                <div class="modal-footer">
                                                                     
                                                                  <button type="button" class="btn grey btn-outline-warning" data-dismiss="modal">Close</button>
                                                        
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>    
                          
                    </div>
                </div>
            </div>
   </div>



@stop

@section('js')

@endsection