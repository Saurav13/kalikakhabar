@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Category Edit</h2>
        </div>
        
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="row">
                      
                        <form action="/admin/category/{{$item->id}}" method="POST" enctype="multipart/form-data">    
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label>Edit Category:</label>
                                <input type="text" class="form-control" name="name" value="{{$item->name}}">
                            </div>

                           
                            <button type="submit" class="btn btn-primary">Save Edit</button>
   
                        </form>
                    </div>
                </div>
            </div>
        </div>

   
   </div>
@stop

@section('js')

@endsection