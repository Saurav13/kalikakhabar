@extends('layouts.admin')

@section('body')
<style>
#newstitle,select{
    font-family:'preetinormal';
    font-size:18px;
}

</style>
@if (count($errors)>0 )
<div class="alert alert-dismissible fade in mb-2">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" style="color:black">&times;</span>
    </button>
    <ul class="list-group">
        @foreach ($errors->all() as $error)
        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
        @endforeach
    </ul>
</div>	
@endif


    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" ><a data-action="collapse">Create News</a></h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        
                        <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            
            <div class="card-body collapse">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="/admin/news"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                
                        <input name="image" type="file" id="upload" class="hidden" onchange="">

                        <div class="form-group">
                                <label for="issueinput3">News Date </label>
                                <input type="date" id="" class="form-control" name="newsdate" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="News Date">
                            </div>
                        

                        <div class="form-group">
                            <label for="category">News Category</label>
                            <select id=" " name="category_id" class="form-control" required>
                                
                                @foreach($items as $item)
                                
                                <option value="{{$item->id}}" >{{$item->name}}</option>
                               
                                @endforeach
                            </select>
                            
                        </div>

                        <div class="form-group">
                            <label for="newstitle">News Title</label>
                            <input type="text" class="form-control" id="newstitle" name="title" required>
                         
                        </div>

                        <div class="form-group">
                            <label for="attachment">Choose News Image</label>
                            <input type="file" class="form-control" id="newsimage" name="news_image" required>
                        </div>

                        <div class="form-group">
                            <label for="mediaurl">Media Url (Optional)</label>
                            <input type="text" class="form-control" id="mediaurl" name="media_url" >
                         
                        </div>

                        <div class="form-group">
                                <label for="message">News First Paragraph <span style="color:red;font-size:25px;">*</span></label>
                                @if ($errors->has('message'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </div>
                                @endif
                                <textarea cols="50" rows="20" class="form-control{{ $errors->has('message') ? ' border-danger' : '' }} " id="newsbody1" name="body1"></textarea>
                            </div>


                            <div class="form-group">
                                    <label for="message">News Second Paragraph <span style="color:red;font-size:25px;">*</span></label>
                                    @if ($errors->has('message'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('message') }}</strong>
                                        </div>
                                    @endif
                                    <textarea cols="50" rows="20" class="form-control{{ $errors->has('message') ? ' border-danger' : '' }}" id="newsbody2" name="body2"></textarea>
                                </div>
    

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>

           
            
        </div>

        
    </div>
        <div class="content-body">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Created News</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><form action="{{ route('news.search') }}" method="GET" class="main-menu-header">
                                    <input type="text" placeholder="Search" name="search" class="menu-search form-control round" value="{{ Request::get('search') ? Request::get('search') : '' }}"/>
                                    <button type="submit" hidden></button>
                                </form>
                            </li>
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>

                
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>News Posted Date</th>
                                        <th>Category</th>
                                        <th>News Title</th>
                                        <th>Photo</th>
                                        <th>Media URL</th>
                                        <th>Reach</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($news as $new)
                                    <tr>
                                        <td>{{$new->created_at->format('d/m/Y ')}}</td>
                                        <td style="font-family:'preetinormal'">{{$new->category->name}}</td>
                                        <td style="font-family:'preetinormal'">{{$new->title}}</td>
                                        <td><img src="/storage/news_image/{{$new->news_image}}" alt="" height="50" width="50"></td>
                                        <td><a href="{{ $new->media_url ? $new->media_url : '#' }}" target="_blank"><i class="{{$new->media_url ? 'fa fa-check-circle' : '' }}" style="font-size:20px"></i></a></td>
                                        <td>{{Counter::show('news',$new->id)}}</td>
                                        <td>
                                            <a href="/admin/news/{{$new->id}}/edit" class="btn btn-outline-warning"><i class="fa fa-pencil"></i></a>
                                            
                                            <form action="/admin/news/{{$new->id}}" method="POST">    
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                
                                                <button type="button" id="deletenews{{$new->id}}" class="btn btn-outline-danger">
                                                    <i class="icon-trash"></i>
                                                </button>
                                            </form>
                                                
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>

                            <div class="text-center">
                                {{$news->links()}}
                            </div>
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
       


@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>

      

       tinymce.init({
       
           selector: "textarea#newsbody1, textarea#newsbody2",

           content_css: [
            '/admin-assets/app-assets/fonts/nepali/pretti.css'],
          
         
    
           plugins: [
               "advlist autolink lists link image charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
           toolbar2: "print preview | forecolor backcolor emoticons | template",
           image_advtab: true,
           file_picker_callback: function(callback, value, meta) {
           if (meta.filetype == 'image') {
               $('#upload').trigger('click');
               $('#upload').on('change', function() {
               var file = this.files[0];
               var reader = new FileReader();
               reader.onload = function(e) {
                   callback(e.target.result, {
                   alt: ''
                   });
               };
               reader.readAsDataURL(file);
               });
           }
           },
           templates: [
               {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
             ]
             
       });
   </script>
  
@endsection