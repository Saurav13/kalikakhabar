@extends('layouts.admin')

@section('body')

<style>
    

</style>

@if (count($errors)>0 )
<div class="alert alert-dismissible fade in mb-2">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" style="color:black">&times;</span>
    </button>
    <ul class="list-group">
        @foreach ($errors->all() as $error)
        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
        @endforeach
    </ul>
</div>	
@endif
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Advertise</h2>
        </div>
        
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="row">
                        <form action="/admin/advertise" method="POST" enctype="multipart/form-data">    
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="adv_type">Advertise Postion</label>
                                <select id=" " name="adv_type" class="form-control" required>
                                  
                                   
                                    <option value="top" >Top</option>
                                    <option value="middle" >Middle</option>
                                    <option value="side" >Side</option>
                                   
                               
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="attachment">Choose Advertise Images</label>
                                <input type="file" class="form-control" id="" name="adv_image">
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                         
                         
                                   
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="card">
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="table-responsive">
                            <table class="table">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>Date of Advertise Image</th>
                                            <th>Advertise Postion</th>
                                            <th>Advertise Image</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($advertises as $advertise)
                                        <tr>
                                            <td>{{$advertise->created_at->format('d/m/Y ')}}</td>
                                            <td>{{$advertise->adv_type}}</td>
                                            <td><img src="/storage/adv_image/{{$advertise->adv_image}}" alt="" height="50" width="50"></td>
                                            <td>
                                                    <button type="button" class="btn btn-outline-danger " data-toggle="modal" data-target="#delModal{{$advertise->id}}">
                                                            Delete
                                                        </button>
                                                        <div class="modal fade text-xs-left" id="delModal{{$advertise->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                  <div class="modal-content">
                                                                    <div class="modal-header">
                                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                      </button>
                                                                      <h4 class="modal-title" id="myModalLabel2"><i class="icon-trash"></i> Are u Sure ?</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                     <center> <h5 class="text-center">Changes cannot be undone !!!</h5>
                                                                    </center>
                                                                    
                                                                      
                                                                     <form action="/admin/advertise/{{$advertise->id}}" method="POST" enctype="multipart/form-data" >    
                                                                        {{ csrf_field() }}
                                                                        {{ method_field('DELETE') }}
                                                                        
                                                                        <center> <button type="submit" class="btn btn-danger "> <i class="icon-trash"></i> Delete</button></center>
                                                                    </form>
                                                                          
                                                                     
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                         
                                                                      <button type="button" class="btn grey btn-outline-warning" data-dismiss="modal">Close</button>
                                                            
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>


                                                              <button type="button" class="btn btn-outline-warning " data-toggle="modal" data-target="#editModal{{$advertise->id}}">
                                                                    Edit
                                                                </button>
                                                                <div class="modal fade text-xs-left" id="editModal{{$advertise->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                                                        <div class="modal-dialog" role="document">
                                                                          <div class="modal-content">
                                                                            <div class="modal-header">
                                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                              </button>
                                                                              <h4 class="modal-title" id="myModalLabel2"><i class="icon-edit"></i> Advertise Edit </h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                              <h5 class="">Edit Advertise</h5>
                                                                    
                                                                                   
                                                                              <form action="/admin/advertise/{{$advertise->id}}" method="POST" enctype="multipart/form-data">    
                                                                                {{ csrf_field() }}
                                                                                {{ method_field('PUT')}}
                                                                                <div class="form-group">
                                                                                    <label for="adv_type">Advertise Postion</label>
                                                                                    <select id=" " name="adv_type" class="form-control" required>
                                                                                      
                                                                                       
                                                                                        <option value="top" >Top</option>
                                                                                        <option value="middle" >Middle</option>
                                                                                        <option value="side" >Side</option>
                                                                                       
                                                                                   
                                                                                    </select>
                                                                                </div>
                                                    
                                                                                <div class="form-group">
                                                                                    <label for="attachment">Choose Advertise Images</label>
                                                                                    <input type="file" class="form-control" id="" name="adv_image">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-primary">Add</button>
                                                                             
                                                                             
                                                                                       
                                                                            </form>
                                                                             
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                 
                                                                              <button type="button" class="btn grey btn-outline-warning" data-dismiss="modal">Close</button>
                                                                    
                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                      </div>

                                            </td>

                                        @endforeach
                            <tr>
                                        
                                    </tbody>
                                </table>
                            </div>    
                          
                    </div>
                </div>
            </div>
   </div>



@stop

@section('js')

@endsection