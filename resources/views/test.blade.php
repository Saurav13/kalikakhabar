
<html>
        <head>
            <title></title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <style type="text/css">
            /* CLIENT-SPECIFIC STYLES */
            body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
            table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
            img { -ms-interpolation-mode: bicubic; }
            
            /* RESET STYLES */
            img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
            table { border-collapse: collapse !important; }
            body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
            
            /* iOS BLUE LINKS */
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
            
            /* MEDIA QUERIES */
            @media screen and (max-width: 480px) {
                .mobile-hide {
                    display: none !important;
                }
                .mobile-center {
                    text-align: center !important;
                }
            }
            
            /* ANDROID CENTER FIX */
            div[style*="margin: 16px 0;"] { margin: 0 !important; }
            </style>
        </head>
        <body style="margin: 0 !important; padding: 0 !important;" bgcolor="#eeeeee">
            
            
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                    <!--[if (gte mso 9)|(IE)]>
                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                    <tr>
                    <td align="center" valign="top" width="600">
                    <![endif]-->
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                        <tr>
                            <td align="left" valign="top" style="font-size:0; padding: 35px; font-family:brandon-grotesque; font-size: 30px;" bgcolor="#ffffff">
                                <a href="{{ route('home') }}"><img width="137" height="47" src="{{ asset('assets/img/emb.jpg') }}" alt="Kalika Khabar"></a>
                                <p style="float:  right;"><strong>Kalika Khabar</strong><br><span style="font-size:15px;text-align:center">Tel: 9851185703</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding: 35px; background-color: #f9f9f9;" bgcolor="#f9f9f9">
            
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                <tr>
                                    <td align="left" style="font-family:brandon-grotesque; font-size: 16px; font-weight: 400; line-height: 24px;">
                                            <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">Hello there,</p>
                                            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi incidunt ducimus, assumenda. Vitae, dolorum perspiciatis.</p>
                                            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, voluptatibus necessitatibus, facilis voluptatum nobis tempora eum. Saepe praesentium non quaerat, deleniti, voluptatum aperiam fugit. Impedit ullam, itaque fuga, expedita, quam rem, magni voluptate vitae nulla vero sunt exercitationem quaerat nesciunt!</p>
                                            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat deleniti, sunt reprehenderit, quod temporibus voluptas corporis magni aperiam fugiat, doloremque consectetur provident ipsam! Aliquam, soluta.</p>
                                            <p><br />Thanks,</p>
                                            <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333; margin: 0;">ShresthaAT Team</p>
                                    </td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="center" style="padding: 35px; background-color: #f7f7f7;" bgcolor="#f7f7f7">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                
                                <tr>
                                    <td align="left" style="font-family:brandon-grotesque; font-size: 14px; font-weight: 400; line-height: 24px;">
                                        <p style="font-size: 14px; font-weight: 400; line-height: 20px; color: #777777;">
                                            If you didn't create an account using this email address, please ignore this email or <a href="" target="_blank" style="color: #777777;">unsusbscribe</a>.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            
            </body>
            </html>
            
            